package co.rmrf.rtspscanner;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    ScannerThread scannerThread = null;
    Toast backToast = null;
    Toast skippedToast = null;
    Toast copiedToast = null;

    private class ScannerThread extends Thread {
        Scanner scanner = null;

        @Override
        public void interrupt() {
            super.interrupt();
            if (scanner != null)
                scanner.stop();
        }

        void skip() {
            if (scanner != null)
                scanner.skip();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initAddress();
        final EditText address = findViewById(R.id.address);
        final ListView list = findViewById(R.id.list);
        final Button scanButton = findViewById(R.id.scan);
        final TextView status = findViewById(R.id.status);
        final ProgressBar progress = findViewById(R.id.progress);

        final ArrayList<String> urls = new ArrayList<>();
        final ArrayAdapter<String> adapter;
        final Database database = new Database(getApplicationContext());
        System.out.println(Arrays.toString(database.getResults()));

        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                urls);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String uri = adapter.getItem(i);
                Intent viewerIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(viewerIntent);
            }
        });
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String uri = adapter.getItem(position);
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("RTSP URI", uri);
                if (clipboard != null) {
                    clipboard.setPrimaryClip(clip);
                    if (copiedToast == null)
                        copiedToast = Toast.makeText(MainActivity.this, "Copied to clipboard", Toast.LENGTH_SHORT);
                    copiedToast.show();
                }
                return true;
            }
        });

        address.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return scanButton.performClick();
            }
        });

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (scannerThread != null && scannerThread.isAlive()) {
                    scannerThread.interrupt();
                    return;
                }
                // WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                // final String ssid = wifiManager != null ? wifiManager.getConnectionInfo().getSSID() : null;
                final String prevButtonText = scanButton.getText().toString();
                urls.clear();
                adapter.notifyDataSetChanged();

                scannerThread = new ScannerThread() {
                    public void run() {
                        try {
                            List<Map.Entry<String, String>> creds = new ArrayList<>();
                            for (String cred : getResources().getStringArray(R.array.credentials)) {
                                String[] pair = cred.split(":");
                                creds.add(new AbstractMap.SimpleEntry<>(pair[0], pair.length > 1 ? pair[1] : ""));
                            }
                            scanner = new Scanner(
                                    address.getText().toString(),
                                    554,
                                    getResources().getStringArray(R.array.urls),
                                    creds,
                                    new Scanner.OnStreamFound() {
                                        @Override
                                        public void streamFound(final String ip, final String uri) {
                                            // database.addResult(ip, uri, ssid);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    urls.add(uri);
                                                    adapter.notifyDataSetChanged();
                                                }
                                            });
                                        }

                                        @Override
                                        public void status(final String ip, final int current, final int total) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    progress.setMax(total);
                                                    progress.setProgress(current);
                                                    status.setText(ip);
                                                }
                                            });
                                        }
                                    });
                            scanner.run();
                        } catch (final Exception e) {
                            e.printStackTrace();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showError(e.getClass().getName(), e.getMessage());
                                }
                            });
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                status.setText(null);
                                progress.setVisibility(View.INVISIBLE);
                                scanButton.setText(prevButtonText);
                            }
                        });
                    }
                };
                scanButton.setText(R.string.stop);
                progress.setVisibility(View.VISIBLE);
                scannerThread.start();
            }
        });

        status.setClickable(true);
        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scannerThread.skip();
                if (skippedToast == null)
                    skippedToast = Toast.makeText(MainActivity.this, "Skipped", Toast.LENGTH_SHORT);
                skippedToast.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (scannerThread != null && scannerThread.isAlive()) {
            scannerThread.interrupt();
        }
    }

    @Override
    public void onBackPressed() {
        if (backToast == null)
            backToast = Toast.makeText(MainActivity.this, "Back button disabled", Toast.LENGTH_SHORT);
        backToast.show();
    }

    private void initAddress() {
        final EditText address = findViewById(R.id.address);
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        if (wifiManager != null) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            int ip = wifiInfo.getIpAddress();
            address.setText(String.format(Locale.ROOT, "%d.%d.%d.0/24", ip & 0xff, ip >> 8 & 0xff, ip >> 16 & 0xff));
        }
    }

    private void showError(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
