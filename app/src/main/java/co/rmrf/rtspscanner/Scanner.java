package co.rmrf.rtspscanner;

import org.apache.commons.net.util.Base64;
import org.apache.commons.net.util.SubnetUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Scanner {
    private static final int AUTH_NONE = 0;
    private static final int AUTH_BASIC = 1;
    private static final int AUTH_DIGEST = 2;
    private String[] addresses;
    private int port = 554;
    private String[] urls;
    private List<Map.Entry<String, String>> creds;
    private OnStreamFound mMessageListener = null;
    private boolean running = false;
    private boolean skip_current = false;

    Scanner(String cidr, int port, String[] urls, List<Map.Entry<String, String>> creds, OnStreamFound listener) {
        if (!cidr.matches(".*/\\d+"))
            cidr += "/32";
        SubnetUtils subnetUtils = new SubnetUtils(cidr);
        subnetUtils.setInclusiveHostCount(true);
        SubnetUtils.SubnetInfo subnetInfo = subnetUtils.getInfo();
        this.port = port;
        this.urls = urls;
        this.creds = creds;
        addresses = subnetInfo.getAllAddresses();
        mMessageListener = listener;
    }

    private static String md5(final String s) {
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                StringBuilder h = new StringBuilder(Integer.toHexString(0xFF & aMessageDigest));
                while (h.length() < 2)
                    h.insert(0, "0");
                hexString.append(h);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    void run() {
        running = true;
        int current = 0;
        int total = addresses.length;
        for (String address : addresses) {
            if (!running)
                break;
            skip_current = false;
            current++;
            mMessageListener.status(address, current, total);
            InetAddress serverAddress;
            try {
                serverAddress = InetAddress.getByName(address);
            } catch (UnknownHostException e) {
                continue;
            }
            for (String url : urls) {
                if (!running || skip_current)
                    break;
                String uri;
                try {
                    if ((uri = tryUrl(serverAddress, port, url)) != null) {
                        mMessageListener.streamFound(address, uri);
                        break;
                    }
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    break;
                }
            }
        }
    }

    void stop() {
        running = false;
    }

    void skip() {
        skip_current = true;
    }

    private String tryUrl(InetAddress address, int port, String url) throws IOException {
        return tryUrl(address, port, url, AUTH_NONE, null, null, false);
    }

    private String tryUrl(InetAddress address, int port, String url, int auth, String user, String pass, boolean retry) throws IOException {
        if (auth != AUTH_NONE)
            System.out.println(String.format(Locale.ROOT, "rtsp://%s:%s@%s:%d%s", user, pass, address.getHostAddress(), port, url));
        else
            System.out.println(String.format(Locale.ROOT, "rtsp://%s:%d%s", address.getHostAddress(), port, url));
        try (Socket socket = new Socket()) {
            socket.setSoTimeout(2500);
            socket.connect(new InetSocketAddress(address, port), 250);
            PrintWriter bufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            BufferedReader bufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            if (!bufferOut.checkError()) {
                bufferOut.print(String.format(Locale.ROOT, "DESCRIBE rtsp://%s:%d%s RTSP/1.0\r\nCSeq: 1\r\n\r\n", address.getHostAddress(), port, url));
                bufferOut.flush();
            }
            String responseLine = bufferIn.readLine();
            if (responseLine == null) {
                return null;
            }
            if (responseLine.matches("^RTSP/[\\d.]+(\\s)+200.*")) {
                return String.format(Locale.ROOT, "rtsp://%s%s:%d%s", auth > AUTH_NONE ? String.format(Locale.ROOT, "%s:%s@", user, pass) : "", address.getHostAddress(), port, url);
            } else if (responseLine.matches("^RTSP/[\\d.]+(\\s)+401.*")) {
                if (auth == AUTH_NONE) {
                    while ((responseLine = bufferIn.readLine()) != null) {
                        if (!running || skip_current)
                            break;
                        if (responseLine.matches("^WWW-Authenticate:(\\s)?+Basic.*")) {
                            auth = AUTH_BASIC;
                            break;
                        }
                        if (responseLine.matches("^WWW-Authenticate:(\\s)?+Digest.*")) {
                            auth = AUTH_DIGEST;
                            break;
                        }
                    }
                } else {
                    String authHeader;
                    if (auth == AUTH_BASIC) {
                        String b64 = Base64.encodeBase64String(String.format(Locale.ROOT, "%s:%s", user, pass).getBytes()); // appends \r\n
                        authHeader = String.format(Locale.ROOT, "Authorization: Basic %s", b64);
                    } else {
                        String realm = null;
                        String nonce = null;
                        String response;
                        while ((responseLine = bufferIn.readLine()) != null) {
                            if (!running || skip_current)
                                break;
                            if (responseLine.matches("^WWW-Authenticate:(\\s)?+Digest.*")) {
                                Pattern realmPattern = Pattern.compile("realm=\"(.*?)\"");
                                Pattern noncePattern = Pattern.compile("nonce=\"(.*?)\"");
                                Matcher realmMatcher = realmPattern.matcher(responseLine);
                                Matcher nonceMatcher = noncePattern.matcher(responseLine);
                                if (!realmMatcher.find() || !nonceMatcher.find()) {
                                    return null;
                                }
                                realm = realmMatcher.group(1);
                                nonce = nonceMatcher.group(1);
                                break;
                            }
                        }
                        if (realm == null || nonce == null)
                            return null;
                        String ha1 = md5(String.format(Locale.ROOT, "%s:%s:%s", user, realm, pass));
                        String ha2 = md5(String.format(Locale.ROOT, "DESCRIBE:rtsp://%s:%d%s", address.getHostAddress(), port, url));
                        response = md5(String.format(Locale.ROOT, "%s:%s:%s", ha1, nonce, ha2));
                        authHeader = String.format(Locale.ROOT, "Authorization: Digest username=\"%s\", realm=\"%s\", nonce=\"%s\", uri=\"rtsp://%s:%d%s\", response=\"%s\"\r\n", user, realm, nonce, address.getHostAddress(), port, url, response);
                    }
                    bufferOut.print(String.format(Locale.ROOT, "DESCRIBE rtsp://%s:%d%s RTSP/1.0\r\n%sCSeq: 2\r\n\r\n", address.getHostAddress(), port, url, authHeader));
                    bufferOut.flush();
                    while ((responseLine = bufferIn.readLine()) != null) {
                        System.out.println(responseLine);
                        if (!running || skip_current)
                            break;
                        if (responseLine.matches("^RTSP/[\\d.]+(\\s)+200.*")) {
                            return String.format(Locale.ROOT, "rtsp://%s:%s@%s:%d%s", user, pass, address.getHostAddress(), port, url);
                        } else if (responseLine.matches("^RTSP/[\\d.].*")) {
                            break;
                        }
                    }
                    return null;
                }
            } else {
                return null;
            }
            bufferOut.close();
            socket.close();
            if (auth != AUTH_NONE) {
                for (Map.Entry<String, String> entry : creds) {
                    if (!running || skip_current)
                        break;
                    String result = tryUrl(address, port, url, auth, entry.getKey(), entry.getValue(), true);
                    if (result != null)
                        return result;
                }
            }
            return null;
        } catch (IOException e) {
            if (!retry)
                throw e;
            return tryUrl(address, port, url, auth, user, pass, false);
        }
    }

    public interface OnStreamFound {
        void status(String ip, int current, int total);

        void streamFound(String ip, String uri);
    }
}
