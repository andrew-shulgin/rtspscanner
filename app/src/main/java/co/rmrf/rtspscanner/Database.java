package co.rmrf.rtspscanner;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Database extends SQLiteOpenHelper {

    Database(Context context) {
        super(context, "rtspscanner.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table urls (id integer primary key, url text)");
        db.execSQL("create table creds (id integer primary key, user text, pass text)");
        db.execSQL("create table results (id integer primary key, ip text, url text, ssid text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    String[] getSsids() {
        SQLiteDatabase db = this.getReadableDatabase();
        final ArrayList<String> ssids = new ArrayList<>();
        Cursor cursor = db.query(true, "results", new String[]{"ssid"}, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ssids.add(cursor.getString(cursor.getColumnIndex("ssid")));
            cursor.moveToNext();
        }
        cursor.close();
        return ssids.toArray(new String[ssids.size()]);
    }

    String[] getResults(String ssid) {
        SQLiteDatabase db = this.getReadableDatabase();
        final ArrayList<String> urls = new ArrayList<>();
        Cursor cursor = db.query("results", new String[]{"url"}, "ssid = ?", new String[]{ssid}, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            urls.add(cursor.getString(cursor.getColumnIndex("url")));
            cursor.moveToNext();
        }
        cursor.close();
        return urls.toArray(new String[urls.size()]);
    }

    String[] getResults() {
        SQLiteDatabase db = this.getReadableDatabase();
        final ArrayList<String> urls = new ArrayList<>();
        Cursor cursor = db.query("results", new String[]{"url"}, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            urls.add(cursor.getString(cursor.getColumnIndex("url")));
            cursor.moveToNext();
        }
        cursor.close();
        return urls.toArray(new String[urls.size()]);
    }

    String[] getUrls() {
        SQLiteDatabase db = this.getReadableDatabase();
        final ArrayList<String> urls = new ArrayList<>();
        Cursor cursor = db.query("urls", new String[]{"url"}, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            urls.add(cursor.getString(cursor.getColumnIndex("url")));
            cursor.moveToNext();
        }
        cursor.close();
        return urls.toArray(new String[urls.size()]);
    }

    Map<String, String> getCreds() {
        SQLiteDatabase db = this.getReadableDatabase();
        final Map<String, String> creds = new HashMap<>();
        Cursor cursor = db.query("creds", new String[]{"user", "pass"}, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            creds.put(cursor.getString(cursor.getColumnIndex("user")), cursor.getString(cursor.getColumnIndex("pass")));
            cursor.moveToNext();
        }
        cursor.close();
        return creds;
    }

    public long addUrl(String url) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("url", url);
        return db.insert("urls", null, values);
    }

    public long addCreds(String user, String pass) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user", user);
        values.put("pass", pass);
        return db.insert("creds", null, values);
    }

    long addResult(String ip, String url, String ssid) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("ip", ip);
        values.put("url", url);
        values.put("ssid", ssid);
        return db.insert("results", null, values);
    }

    public int removeResults(String ssid) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("results", "ssid = ?", new String[]{ssid});
    }

    public int clearResults() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("results", null, null);
    }
}
